# Poetry Python Package Management cheetsheet

* [https://python-poetry.org/docs/#installation](https://python-poetry.org/docs/#installation)
* [https://pypi.org/](https://pypi.org/)

# Install Python and Poetry Package manager

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

Add poetry `$PATH` to your .bashrc:

```bash
echo "export PATH=$PATH:$HOME/.poetry/bin/" >> ~/.bashrc
```

## Update poetry and pip inside virual environment. Install Operating System dependancies.

Install OS dependancies

```bash
pacman -Sy blas lapack gcc-fortran
```

Create new data science project and update standard libraries.

```bash
poetry new ML-Project
poetry self update
poetry run pip install --upgrade pip
```
Now add scikit-learn and jupyterlab for using notebooks:

```bash
poetry add scikit-learn
poetry add npm
poetry add nodejs
poetry add jupyterlab
poetry add jupyterthemes
```

Install jupyter theme:

```bash
poetry run jupyter labextension install @arbennett/base16-gruvbox-dark
```


