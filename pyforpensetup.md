# A Walkthrough for setting up an environment for the TryHackMe room Python for Pentesters.

* [Python for Pentesters](https://www.tryhackme.com/room/pythonforcybersecurity)

# Poetry Python Package Management cheetsheet

* [https://python-poetry.org/docs/#installation](https://python-poetry.org/docs/#installation)
* [https://pypi.org/](https://pypi.org/)

# Install Python and Poetry Package manager

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```
## Update poetry and pip inside virual environment if needed

```bash
poetry self update
```

Later you can use pip command directly inside of your python environment like so:

```bash
poetry run pip install --upgrade pip
```
## Poetry create new project

```bash
poetry new pyforpensetup
```
## Enter project

```bash
cd pyforpensetup
```
## Manually adding python modules using poetry cli
You can use PyPi and libraries.io to discover package versions

* [https://pypi.org/project/tensorflow/2.8.0rc1/](https://pypi.org/project/tensorflow/2.8.0rc1/)
* [https://libraries.io/pypi/tensorflow](https://libraries.io/pypi/tensorflow)

Using poetry lets add all the libraries needed for follow along in the TryHackMe [Python for Pentesters](https://www.tryhackme.com/room/pythonforcybersecurity) room.

Install dependancies to your to your distribution:

Debian, Kali or Ubuntu example:

```bash
sudo apt install nodejs npm build-essential
```
Arch Linux

```bash
pacman -Sy npm nodejs
```

```bash
poetry add requests scapy pyfiglet keyboard paramiko jupyterlab nodejs npm
```

### Add a badass theme using jupyter cli

* [https://github.com/arbennett/jupyterlab-themes](https://github.com/arbennett/jupyterlab-themes)

Pick a theme from above gitlab repository cited above and use the following command to install:

```bash
poetry run jupyter labextension install @arbennett/base16-{$themename}
```
Here is an example of the base16-gruvbox-dark theme.

```bash
poetry run jupyter labextension install @arbennett/base16-gruvbox-dark
```
Here is an example of the base16-outrun theme.

```bash
poetry run jupyter labextension install @arbennett/base16-outrun
```
Here is an example of the Nord theme install

```bash
poetry run jupyter labextension install @arbennett/base16-nord
```
* [https://github.com/Rahlir/theme-gruvbox](https://github.com/Rahlir/theme-gruvbox)

```bash
poetry run jupyter labextension install @rahlir/theme-gruvbox
```

## Fire up Jupyter Lab

```bash
jupyter lab
```

# Other Awesome Python projects

<img src="numpy.png"/>

* [https://keras.io/](https://keras.io/)
* [https://www.tensorflow.org/](https://www.tensorflow.org/)
* [https://scikit-learn.org/stable/index.html](https://scikit-learn.org/stable/index.html)
* [https://pandas.pydata.org/](https://pandas.pydata.org/)
* [https://scipy.org/](https://scipy.org/)
* [https://numpy.org/](https://numpy.org/)
* [https://qutip.org/](https://qutip.org/)
* [https://pyquil-docs.rigetti.com/en/stable/](https://pyquil-docs.rigetti.com/en/stable/)
* [https://qiskit.org/](https://qiskit.org/)


