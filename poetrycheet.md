# Poetry Python Package Management cheetsheet

* [https://python-poetry.org/docs/#installation](https://python-poetry.org/docs/#installation)
* [https://pypi.org/](https://pypi.org/)

# Install Python and Poetry Package manager

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```
## Update poetry and pip inside virual environment if needed

```bash
poetry self update
poetry run pip install --upgrade pip
```
# Pip use of Sdist vs Wheel

Poetry uses pip for installing packages from [https://pypi.org/](https://pypi.org/) pip can install from either `Source Distributions (sdist)` or `Wheels`, but if both are present on PyPI, pip will prefer a compatible wheel. You can override pips default behavior by e.g. using its `–no-binary` option. Wheels are a pre-built distribution format that provides faster installation compared to Source Distributions (sdist), especially when a project contains compiled extensions. If pip does not find a wheel to install, it will locally build a wheel and cache it for future installs, instead of rebuilding the source distribution in the future. Some packages such as `numpy` might need wheel upgraded.

```bash
poetry run pip install --upgrade pip setuptools wheel
```
## Poetry create new project

```bash
poetry new poetryfoo
```
## Initialize new project

```bash
cd poetryfoo
poetry init
```
## Manually adding python modules using poetry cli
You can use PyPi and libraries.io to discover package versions

* [https://pypi.org/project/tensorflow/2.8.0rc1/](https://pypi.org/project/tensorflow/2.8.0rc1/)
* [https://libraries.io/pypi/tensorflow](https://libraries.io/pypi/tensorflow)

Note: python and pytest are automagically added already. Some example additions.

```bash
poetry add pandas
poetry add jupyterlab
poetry add tensorflow@^2.1.0rc0
poetry add tensorflow@^2.8.0rc1
```

## Numpy project example toml generation

### Python dependancies
Versions of dependancies will need to be either increased or decreased until all can function together. As of 30JAN22 the following TOML file allows Python, SciPY and Numpy to work together without a dependancy issue based on listed versions. Appending additional packages may require some moderations to these versions until compatibility can be achieved once again.
### Host Operating System dependancies
Pay attention to error when dependancies fail. Many of these errors or warnings are not due to Python dependancies but missing host operating system libraries, binaries or tools needed. For example to install SciPy the following host operating system, in this case Arch Linux, will need to be installed first or SciPy will not compile.

#### Arch Linux based

```bash
sudo pacman -Sy openblas lapack gcc-fortran gcc10-fortran gcc10-libs qd
```
#### Debian based

```bash
apt install build-essential
```


```bash
tool.poetry]
name = "machinel"
version = "0.1.0"
description = ""
authors = ["trentonknight <trentonknight@protonmail.com>"]

[tool.poetry.dependencies]
python = "3.7"
scipy = "^1.7.3"
numpy = "1.21"
[tool.poetry.dev-dependencies]
pytest = "*"

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
```
## Fire up Jupyter Lab

```bash
jupyter lab
```
# Qiskit setup

```bash
poetry add jupyterlab npm nodejs jupyterthemes qiskit numpy
```
# TOML configuration examples

Some python libraries may need build tools like, make, gcc, headers, or any other host based dependancies. Make sure to examine errors as much is not related to python dependancies.

## Example for install Scipy 

```bash
sudo pacman -Sy openblas lapack gcc-fortran gcc10-fortran gcc10-libs qd
```
TOML file needed. Python version reduced to 3.7.

```toml
[tool.poetry]
name = "qtest"
version = "0.1.0"
description = ""
authors = ["trentonknight <trentonknight@protonmail.com>"]

[tool.poetry.dependencies]
python = "3.7"
scipy = "^1.7.3"

[tool.poetry.dev-dependencies]
pytest = "*"

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
```
## Update package after TOML edits

```bash
poetry update package
```
Append Qiskit

```bash
poetry add qiskit
```

# Optional additions
## Themes and other eye candy

```bash
poetry add npm
poetry add nodejs
poetry add jupyterthemes
```
### Or in Arch LInux use the following

```bash
pacman -Sy npm nodejs
```

### Finally add theme using jupyter cli

* [https://github.com/arbennett/jupyterlab-themes](https://github.com/arbennett/jupyterlab-themes)

Pick a theme from above gitlab repository cited above and use the following command to install:

```bash
poetry run jupyter labextension install @arbennett/base16-{$themename}
```
Here is an example of the base16-gruvbox-dark theme.

```bash
poetry run jupyter labextension install @arbennett/base16-gruvbox-dark
```
Here is an example of the base16-outrun theme.

```bash
poetry run jupyter labextension install @arbennett/base16-outrun
```
Here is an example of the Nord theme install

```bash
poetry run jupyter labextension install @arbennett/base16-nord
```
## Gruvbox themes
* [https://github.com/Rahlir/theme-gruvbox](https://github.com/Rahlir/theme-gruvbox)

```bash
poetry run jupyter labextension install @rahlir/theme-gruvbox
```

# Python projects

<img src="numpy.png"/>

* [https://keras.io/](https://keras.io/)
* [https://www.tensorflow.org/](https://www.tensorflow.org/)
* [https://scikit-learn.org/stable/index.html](https://scikit-learn.org/stable/index.html)
* [https://pandas.pydata.org/](https://pandas.pydata.org/)
* [https://scipy.org/](https://scipy.org/)
* [https://numpy.org/](https://numpy.org/)
* [https://qutip.org/](https://qutip.org/)
* [https://pyquil-docs.rigetti.com/en/stable/](https://pyquil-docs.rigetti.com/en/stable/)
* [https://qiskit.org/](https://qiskit.org/)


